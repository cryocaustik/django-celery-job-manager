from celery import shared_task
import time
from miner.models import Person


@shared_task
def hello():
    print("Hello there!")


@shared_task
def bye():
    time.sleep(30)
    print("googbye")


@shared_task
def add_person():
    p = Person(
        name="Alex Sky", gender="M", social_media="https://facebook.com/u/oi325n2lk223"
    )
    p.save()
    return p
