from django.db import models
from datetime import datetime
from django.utils import timezone

# Create your models here.
class Person(models.Model):
    name = models.CharField(max_length=200)
    gender = models.CharField(max_length=200)
    social_media = models.URLField()
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)
